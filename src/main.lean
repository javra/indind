import .util

open tactic

namespace indind

section cats
  parameters (n : name) (sorts constrs : list expr) (tgt_lvl : level)

meta def sort_names : list name := sorts.map expr.local_uniq_name

meta def add_Alg : tactic name := do
  let na := n ++ `Alg,
  add_inductive na [] 0 (expr.sort tgt_lvl)
    [(na ++ `mk, expr.pis (sorts ++ constrs) (expr.const na []))],
  return na

meta def flatten_sort : expr → expr
| (expr.pi _ _ _ b)           := flatten_sort b
| (expr.local_const m n bi t) := expr.local_const m n bi (flatten_sort t)
| (expr.app f x)              := expr.app (flatten_sort f) (flatten_sort x)
| e                           := e

meta def pre_sorts := sorts.map flatten_sort

meta def flatten_constr_at (s : expr) (e : expr) : expr :=
e.replace (λ e' d, if e'.is_lapp_of s.local_uniq_name
                   then some s
                   else none)

meta def pre_constrs : list expr :=
constrs.map $ (λ e, list.foldr flatten_constr_at e pre_sorts)

-- Add prealgebra structure
meta def add_Prealg : tactic name := do
  let na := n ++ `Prealg,
  add_inductive na [] 0 (expr.sort tgt_lvl)
    [(na ++ `mk, (expr.const na []).pis (pre_sorts ++ pre_constrs))],
  return na

-- Add a finite type that indexes the sorts.
meta def add_SI : tactic name := do
  let na := n ++ `SI,
  add_inductive na [] 0 `(Type) $
    sorts.map (λ e, (na ++ e.local_uniq_name, expr.const na [])),
  return na

meta def e_SI : expr := expr.const (n ++ `SI) []

meta def S'_constr_fix_at (n'' : name) (e : expr) : expr :=
e.replace (λ e' d, match e' with
                   | expr.local_const m n' bi t :=
                     if (n'' = n')
                     then some (expr.app 
                       (expr.const (n ++ `S') [])
                       (expr.const (n ++ `SI ++ n') []))
                     else none
                   | _ := none
                   end)

-- The constructors for the initial untyped algebra
meta def S'_constrs : list expr := pre_constrs.map $ (λ e, list.foldr S'_constr_fix_at e sort_names)

-- Add the inductively defined initial untyped algebra
meta def add_S' : tactic name := do
  let na := n ++ `S',
  --trace $ S'_constrs.map (λ (e : expr), (na ++ e.local_uniq_name, e.local_type)),
  add_inductive na [] 0 (expr.pi `b binder_info.default e_SI `(Type)) $
    S'_constrs.map (λ (e : expr), (na ++ e.local_uniq_name, e.local_type)),
  return na

meta def e_S' : expr := expr.const (n ++ `S') []

-- This changes the type of a sort to a pi type with unit as its body
meta def make_body_unit : expr → expr
--| (expr.local_const m n bi b) := make_body_unit b
| (expr.pi n bi h b)          := expr.pi n bi h (make_body_unit b)
| (expr.sort l)               := `(unit)
| e                           := e

-- This replaces any local constants by their proper indexed S' fiber
-- TODO make this more robust
meta def replace_local_consts_by_SI_app (e : expr) : expr :=
e.replace (λ e d,
match e with
| (expr.local_const m n' bi b) := some (expr.app 
                       (expr.const (n ++ `S') [])
                       (expr.const (n ++ `SI ++ n') []))
| e := none
end)

-- TODO make robust (don't just match on sort!!!)
-- Add the type of the arguments of the welltypeness predicate
meta def add_Sarg : tactic unit := do
  let na := n ++ `Sarg,
  let es : list (name × expr) := sorts.map $ λ s,
    let e := (replace_local_consts_by_SI_app s.local_type).replace $ λ e d, match e with
      | expr.sort _               := some $ (expr.app (expr.const na []) ((expr.const (n ++ `SI ++ s.local_uniq_name) [])))
      | _                         := none
      end in
    (n ++ `Sarg ++ s.local_uniq_name, e),
  add_inductive na [] 0 (expr.pi `b binder_info.default e_SI `(Type)) es 

meta def e_Sarg : expr := expr.const (n ++ `Sarg) []

meta def Sw_type : expr :=
expr.pi `b binder_info.default e_SI $
expr.pi `x binder_info.default (expr.app e_S' (expr.var 0)) $
expr.pi `gx binder_info.default (expr.app e_Sarg (expr.var 1)) `(Prop)

meta def get_sort_name_and_args : expr → (name × list expr)
| (expr.pi n bi h b)           := get_sort_name_and_args b
| (expr.local_const m n nbi t) := (m, [])
| (expr.app f x)               := ((get_sort_name_and_args f).1, x::(get_sort_name_and_args f).2)
| e                            := (name.anonymous, [])

meta def S'ify_constr (e : expr) : expr :=
e.replace $ λ e _, match e with
                   | (expr.local_const m _ _ _) := some $ expr.const (n ++ `S' ++ m) []
                   | _ := none
                   end

--TODO reuse pre-constructors instead?
--TODO do this without var lifting
meta def mk_Sw_constr (e : expr) : tactic expr := do
  let n_sw := n ++ `Sw,
  let hs := e.collect_headers,
  let s := get_sort_name_and_args e.local_type,
  let swprefix := n ++ `gg, --mk_fresh_name,
  -- hs' are only index arguments, not parameter arguments
  let hs' := hs.filter $ λ (e : expr),
    (get_sort_name_and_args $ e.local_type.lift_vars 0 hs.length).1 ≠ name.anonymous,
  let sw : list expr := hs'.map $
    λ e, let (ns, ps) := get_sort_name_and_args $ e.local_type.lift_vars 0 hs.length in
      expr.local_const (swprefix ++ ns) (swprefix ++ ns) binder_info.default $
      expr.app (expr.app (expr.app (expr.const n_sw [])
      (expr.const (n ++ `SI ++ ns) []))
      e)
      (expr.apps (expr.const (n ++ `Sarg ++ ns) []) ps.reverse), --!!!
  let SIconst : expr := expr.const (n ++ `SI ++ s.1) [],
  let body := expr.app (expr.app (expr.app (expr.const n_sw []) SIconst) $
    (hs.foldl expr.app (expr.const (n ++ `S' ++ e.local_uniq_name) []))) 
    (expr.apps (expr.const (n ++ `Sarg ++ s.1) []) s.2.reverse),
  let lbody := (body.lift_vars 0 hs'.length),
  let lbody' := list.foldr S'_constr_fix_at (list.foldr flatten_constr_at (lbody.pis (hs ++ sw)) pre_sorts) sort_names,
  return $ S'ify_constr lbody'

-- Add the welltypedness predicate
meta def add_Sw : tactic name := do 
  let na := n ++ `Sw,
  cs ← constrs.mmap $ λ e, (do
    constr ← mk_Sw_constr e,
    return (n ++ `Sw ++ e.local_uniq_name, constr)),
  add_inductive na [] 0 Sw_type cs,
  return na

meta def e_Sw : expr := expr.const (n ++ `Sw) []

meta def add_S_sorts : tactic (list name) := do
  sorts.for_each $ λ s, (do
    let sn := s.local_uniq_name,
    let st := s.local_type.replace $
    λ e d, match e with
           | (expr.local_const m n bi t) := some $ expr.const m []
           | _ := none
           end,
    add_theorem_by sn [] st $ (do 
      sp ← intros,
      let si : expr := expr.const (n ++ `SI ++ sn) [],
      let fs : expr := expr.app e_S' si,
      firsts ← sp.mmap $ λ e, mk_mapp `psigma.fst [none, none, some e],
      bname ← mk_fresh_name,
      let fam : expr := expr.lam bname binder_info.default fs $
        expr.app (expr.app (expr.app e_Sw si) (expr.var 0)) $
        expr.apps (expr.const (n ++ `Sarg ++ sn) []) $
        firsts,
      sig ← mk_mapp `psigma [none, some fam],
      exact sig,
      return ()),
    return ()),
  return []

meta def is_sort_app : expr → bool
| (expr.app f x)             := is_sort_app f
| (expr.local_const m n b t) := m ∈ sort_names
| (expr.const m l)           := m ∈ sort_names
| _                          := ff

meta def add_S_constrs : tactic unit := do
  constrs.for_each $ λ c, do
  let cn := c.local_uniq_name,
  let sn := prod.fst $ get_sort_name_and_args c.local_type,
  let ct := c.local_type.replace $
  λ e d, match e with
         | (expr.local_const m n bi t) := some $ expr.const m []
         | _ := none
         end,
  add_theorem_by cn [] ct $ (do
    sp ← intros,
    sp1 : list expr ← sp.mmap $ λ s, (do st ← infer_type s,
                                     if is_sort_app st
                                     then mk_app `psigma.fst [s]
                                     else return s),
    sp2 : list expr ← sp.mmap $ λ s, (do st ← infer_type s,
                                     if is_sort_app st
                                     then mk_app `psigma.snd [s]
                                     else return s),
    fsts ← mk_app (n ++ `S' ++ cn) sp1,
    snds ← mk_app (n ++ `Sw ++ cn) sp2,
    dunfold_target [sn],
    fconstructor,
    exact fsts,
    exact snds),
  return ()

meta def e_S : expr := expr.const (n ++ `S) []

meta def add_ind_helpers : tactic unit := do
  add_Alg,
  add_Prealg,
  add_SI,
  add_S',
  add_Sarg,
  add_Sw,
  add_S_sorts,
  add_S_constrs,
  return ()

meta def add_Ms_to_names : expr → expr
| (expr.pi m bi t b)           :=  if t.is_app_of_local_const
                                   then expr.pi (n ++ `M ++ m) bi (add_Ms_to_names t) (add_Ms_to_names b)
                                   else expr.pi m bi (add_Ms_to_names t) (add_Ms_to_names b)
| (expr.local_const m n' bi t) := expr.local_const (n ++ `M ++ m) (n ++ `M ++ n') bi (add_Ms_to_names t)
| (expr.app f x)               := expr.app (add_Ms_to_names f) (add_Ms_to_names x)
| e                            := e

meta def motive_sorts : list expr := sorts.map (add_Ms_to_names ∘ expr.make_local_implicit)
meta def motive_constrs : list expr := constrs.map (add_Ms_to_names ∘ expr.make_local_implicit)

meta def close_motive_args : expr → expr := λ e, e.pis $ motive_sorts ++ motive_constrs
meta def app_motive_args : expr → expr := λ e, e.apps $ motive_sorts ++ motive_constrs

meta def intro_params : tactic (list expr × list expr) :=
do ls : list expr ← motive_sorts.mmap $ λ s, intro s.local_uniq_name,
   lc : list expr ← motive_constrs.mmap $ λ c, intro c.local_uniq_name,
   return (ls, lc)

-- TODO make robust (don't just match on sort!!!)
-- Add the type of the arguments of the welltypeness predicate
meta def add_Rarg : tactic unit := do
  let na := n ++ `Rarg,
  es : list (name × expr) ← sorts.mmap $ λ s, (do
    let hs := s.collect_headers,
    let sn := s.local_uniq_name,
    let rarg_app : expr := expr.app (app_motive_args (expr.const na [])) $ expr.const (n ++ `SI ++ sn) [],
    let M_app : expr := (expr.local_const (n ++ `M ++ sn) (n ++ `M ++ sn) binder_info.default (expr.const `Prop [])).apps hs,
    let b : expr := expr.pi `x binder_info.default M_app rarg_app,
    let b' := b.pis $ hs.map $
      λ e, expr.local_const e.local_uniq_name e.local_pp_name e.local_binder_info $ add_Ms_to_names e.local_type,
    return (n ++ `Rarg ++ sn, close_motive_args b')),
  add_inductive na [] (motive_sorts.length + motive_constrs.length)
    (close_motive_args (expr.pi `b binder_info.default e_SI `(Type))) es

meta def e_Rarg : expr := expr.const (n ++ `Rarg) []
meta def e_Rarg_m : expr := app_motive_args e_Rarg

meta def add_rel : tactic unit := do
  let na := n ++ `rel,
  let t : expr := expr.pi `b binder_info.default e_SI $
     expr.pi `x binder_info.default (expr.app e_S' (expr.var 0)) $
     expr.pi `x binder_info.default (expr.app e_Rarg_m (expr.var 1)) $ expr.sort level.zero,
  cs : list (name × expr) ← constrs.mmap $ λ e, (do
    let cn := e.local_uniq_name,
    let hs :list expr := e.collect_headers.map $ λ e,
      match e with
      | (expr.local_const m n' bi t) := 
        let sn := (get_sort_name_and_args e.local_type).1 in
        if sn = name.anonymous
        then expr.local_const m n' bi t
        else expr.local_const m n' bi (expr.app (expr.const (n ++ `S') []) (expr.const (n ++ `SI ++ sn) []))
      | _ := e
      end,
    let s := get_sort_name_and_args e.local_type,
    -- hs' doesn't contain external parameters
    let hs' := e.collect_headers.filter $ λ e, e.local_type.is_app_of_local_const,
    -- the motive(?) we need
    let m_local := add_Ms_to_names e,
    let m_args := s.2.reverse.map $ λ e, add_Ms_to_names $ e.lift_vars 0 hs'.length,
    -- the motive's headers
    let mhs : list expr := m_local.collect_headers,
    let mhs' := mhs.filter $ λ e, (get_sort_name_and_args $ e.local_type.lift_vars 0 hs.length).1 ≠ name.anonymous,
    let mb := expr.apps m_local mhs,
    -- the rel arguments
    relargs : list expr ← hs'.mmap $ λ e, (do
      let (ns, ps) := get_sort_name_and_args $ e.local_type.lift_vars 0 hs.length,
      fn ← mk_fresh_name,
      return $ expr.local_const fn fn binder_info.default $
      (expr.app 
       (expr.app 
        (expr.app (app_motive_args $ expr.const na []) (expr.const (n ++ `SI ++ ns) []))
        (expr.local_const e.local_uniq_name e.local_uniq_name binder_info.default (`(Type) : expr)))
       (expr.app
        (expr.apps (app_motive_args $ expr.const (n ++ `Rarg ++ ns) []) ps.reverse)
        (expr.var (hs.length - 1))))),
    let cb : expr := expr.app (expr.app (expr.app (app_motive_args $ expr.const na [])
        (expr.const (n ++ `SI ++ s.1) []))
        (expr.apps (expr.const (n ++ `S' ++ cn) []) hs)) $
      expr.apps (app_motive_args $ expr.const (n ++ `Rarg ++ s.1) []) m_args mb,
    let ret := close_motive_args $ expr.pis (hs ++ mhs' ++ relargs) cb,
    return (na ++ cn, ret)),
  add_inductive na [] (motive_sorts.length + motive_constrs.length)
    (close_motive_args t) cs

meta def e_rel : expr := expr.const (n ++ `rel) []
meta def e_rel_m : expr := app_motive_args e_rel

meta def add_rel_unique : tactic expr := do
  let t := close_motive_args $ expr.pi `b binder_info.default e_SI $
    expr.pi `x binder_info.default (expr.app e_S' $ expr.var 0) $
    expr.pi `a binder_info.default (expr.app e_Rarg_m $ expr.var 1) $
    expr.pi `a' binder_info.default (expr.app e_Rarg_m $ expr.var 2) $
    expr.pi `ar binder_info.default (expr.apps e_rel_m [expr.var 3, expr.var 2, expr.var 1]) $
    expr.pi `ar' binder_info.default (expr.apps e_rel_m [expr.var 4, expr.var 3, expr.var 1]) $
    expr.apps (expr.const `eq [level.succ level.zero]) [expr.app e_Rarg_m (expr.var 5), expr.var 3, expr.var 2],
  add_theorem_by (n ++ `rel_unique) [] t (do
    M_args ← intro_params,
    b ← intro `b,
    x ← intro `x,
    a ← intro `a,
    a' ← intro `a',
    ar ← intro `ar,
    ar' ← intro `ar',
    is ← induction x,
    /-is.for_each $ λ ⟨_, e, ps⟩, do
      bb ← get_local `ar,
      induction bb [] `DC.rel.rec,
      return (),-/
    trace_state,
    admit )

#check interactive.cases_arg_p
#check interactive.types.with_ident_list
#check lean.parser_state

meta def add_rel_helpers : tactic unit :=
do add_Rarg,
   add_rel,
   add_rel_unique,
   return ()

meta def add_to_env : tactic unit :=
do add_ind_helpers,
   add_rel_helpers,
   return ()

end cats

end indind

meta def bidef := binder_info.default