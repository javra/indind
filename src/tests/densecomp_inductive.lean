import ..main

section
  --parameters (A : Type) (lt : A → A → Type)
  def A := nat
  inductive lt : A → A → Type
  | mk : Π m n, nat.lt m n → lt m n

meta def e_A' : expr := expr.local_const `A' `A' bidef `(Type)
meta def e_lt' : expr := expr.local_const `lt' `lt' bidef $
  expr.pi `x bidef e_A' $
  expr.pi `y bidef e_A' `(Type)

meta def e_iota_A : expr := expr.local_const `iota_A `iota_A bidef $
  expr.pi `a bidef (expr.const `A []) e_A' 
meta def e_mid : expr := expr.local_const `mid `mid bidef $
  expr.pi `x bidef e_A' $
  expr.pi `y bidef e_A' $
  expr.pi `p bidef (expr.app (expr.app e_lt' (expr.var 1)) (expr.var 0)) e_A'
meta def e_iota_lt : expr := expr.local_const `iota_lt `iota_lt bidef $
  expr.pi `a bidef (expr.const `A []) $
  expr.pi `b bidef (expr.const `A []) $
  expr.pi `p bidef (expr.app (expr.app (expr.const `lt []) (expr.var 1)) (expr.var 0)) $
  expr.app (expr.app e_lt' (expr.app e_iota_A (expr.var 2))) (expr.app e_iota_A (expr.var 1))
meta def e_mid_l : expr := expr.local_const `mid_l `mid_l bidef $
  expr.pi `x bidef e_A' $
  expr.pi `y bidef e_A' $
  expr.pi `p bidef (expr.app (expr.app e_lt' (expr.var 1)) (expr.var 0)) $
  expr.app (expr.app e_lt' (expr.var 2)) (expr.app (expr.app (expr.app e_mid (expr.var 2)) (expr.var 1)) (expr.var 0))
meta def e_mid_r : expr := expr.local_const `mid_r `mid_r bidef $
  expr.pi `x bidef e_A' $
  expr.pi `y bidef e_A' $
  expr.pi `p bidef (expr.app (expr.app e_lt' (expr.var 1)) (expr.var 0)) $
  expr.app (expr.app e_lt' (expr.app (expr.app (expr.app e_mid (expr.var 2)) (expr.var 1)) (expr.var 0))) (expr.var 1)

meta def sorts := [e_A', e_lt']
meta def constrs := [e_iota_A, e_mid, e_iota_lt, e_mid_l, e_mid_r]

set_option pp.proofs true
set_option trace.app_builder true

def bar : Type :=
begin
  indind.add_to_env `DC sorts constrs level.zero,
  exact unit,
end

meta instance: has_repr expr := ⟨expr.to_string⟩

#print DC.Alg
#print DC.Prealg.mk
#print DC.SI
#print DC.S'
#print DC.Sarg
#print DC.Sw
#print A'
#print lt'
#print iota_A
#print iota_lt
#print mid
#print mid_l
#print DC.Rarg
#print DC.rel
#print DC.rel_unique

#exit

inductive rel' {DCMA' : Type} {DCMlt' : DCMA' → DCMA' → Type} {DCMiota_A : A → DCMA'}
{DCMmid : Π (DCMx DCMy : DCMA'), DCMlt' DCMx DCMy → DCMA'}
{DCMiota_lt : Π (a b : A), lt a b → DCMlt' (DCMiota_A a) (DCMiota_A b)}
{DCMmid_l :
  Π (DCMx DCMy : DCMA') (DCMp : DCMlt' DCMx DCMy), DCMlt' DCMx (DCMmid DCMx DCMy DCMp)}
{DCMmid_r :
  Π (DCMx DCMy : DCMA') (DCMp : DCMlt' DCMx DCMy), DCMlt' (DCMmid DCMx DCMy DCMp) DCMy} :
Π (b : DC.SI), DC.S' b → @DC.Rarg DCMA' DCMlt' DCMiota_A DCMmid 
 DCMiota_lt DCMmid_l DCMmid_r b → Prop
| iota_A : Π (a : A), rel' DC.SI.A' (DC.S'.iota_A a) (DC.Rarg.A' (DCMiota_A a))
| mid : ∀ (x y : DC.S' DC.SI.A') (p : DC.S' DC.SI.lt') (DCMx DCMy : DCMA') (DCMp : DCMlt' DCMx DCMy),
  rel' DC.SI.A' x (@DC.Rarg.A'  DCMA' DCMlt' DCMiota_A DCMmid 
 DCMiota_lt DCMmid_l DCMmid_r DCMx) →
  rel' DC.SI.A' y (@DC.Rarg.A'  DCMA' DCMlt' DCMiota_A DCMmid 
 DCMiota_lt DCMmid_l DCMmid_r DCMy) →
  rel' DC.SI.lt' p (@DC.Rarg.lt'  DCMA' DCMlt' DCMiota_A DCMmid 
 DCMiota_lt DCMmid_l DCMmid_r DCMx DCMy DCMp) →
  rel' DC.SI.A' (DC.S'.mid x y p) (@DC.Rarg.A'  DCMA' DCMlt' DCMiota_A DCMmid 
 DCMiota_lt DCMmid_l DCMmid_r (DCMmid DCMx DCMy DCMp))
| iota_lt : ∀ (a b : A) (p : lt a b),
 rel' DC.SI.lt' (DC.S'.iota_lt a b p) (@DC.Rarg.lt'  DCMA' DCMlt' DCMiota_A DCMmid 
 DCMiota_lt DCMmid_l DCMmid_r (DCMiota_A a) (DCMiota_A b) (DCMiota_lt a b p))
| mid_l : ∀ (x y : DC.S' DC.SI.A') (p : DC.S' DC.SI.lt') (DCMx DCMy : DCMA') (DCMp : DCMlt' DCMx DCMy),
  rel' DC.SI.A' x (@DC.Rarg.A'  DCMA' DCMlt' DCMiota_A DCMmid 
 DCMiota_lt DCMmid_l DCMmid_r DCMx) →
  rel' DC.SI.A' y (@DC.Rarg.A' DCMA' DCMlt' DCMiota_A DCMmid 
 DCMiota_lt DCMmid_l DCMmid_r DCMy) →
  rel' DC.SI.lt' p (@DC.Rarg.lt' DCMA' DCMlt' DCMiota_A DCMmid 
 DCMiota_lt DCMmid_l DCMmid_r DCMx DCMy DCMp) →
  rel' DC.SI.lt' (DC.S'.mid_l x y p)
    (@DC.Rarg.lt' DCMA' DCMlt' DCMiota_A DCMmid 
 DCMiota_lt DCMmid_l DCMmid_r DCMx (DCMmid DCMx DCMy DCMp) (DCMmid_l DCMx DCMy DCMp))
| mid_r : ∀ (x y : DC.S' DC.SI.A') (p : DC.S' DC.SI.lt') (DCMx DCMy : DCMA') (DCMp : DCMlt' DCMx DCMy),
  rel' DC.SI.A' x (@DC.Rarg.A' DCMA' DCMlt' DCMiota_A DCMmid 
 DCMiota_lt DCMmid_l DCMmid_r DCMx) →
  rel' DC.SI.A' y (@DC.Rarg.A' DCMA' DCMlt' DCMiota_A DCMmid 
 DCMiota_lt DCMmid_l DCMmid_r DCMy) →
  rel' DC.SI.lt' p (@DC.Rarg.lt' DCMA' DCMlt' DCMiota_A DCMmid 
 DCMiota_lt DCMmid_l DCMmid_r DCMx DCMy DCMp) →
  rel' DC.SI.lt' (DC.S'.mid_r x y p)
    (@DC.Rarg.lt' DCMA' DCMlt' DCMiota_A DCMmid 
 DCMiota_lt DCMmid_l DCMmid_r (DCMmid DCMx DCMy DCMp) DCMy (DCMmid_r DCMx DCMy DCMp))

set_option trace.check true
example (DCMA' : Type)
  (DCMlt' : DCMA' → DCMA' → Type)
  (DCMiota_A : A → DCMA')
  (DCMmid : Π (DCMx DCMy : DCMA'), DCMlt' DCMx DCMy → DCMA')
  (DCMiota_lt : Π (a b : A), lt a b → DCMlt' (DCMiota_A a) (DCMiota_A b))
  (DCMmid_l :
  Π (DCMx DCMy : DCMA') (DCMp : DCMlt' DCMx DCMy), DCMlt' DCMx (DCMmid DCMx DCMy DCMp))
  (DCMmid_r :
  Π (DCMx DCMy : DCMA') (DCMp : DCMlt' DCMx DCMy), DCMlt' (DCMmid DCMx DCMy DCMp) DCMy)
  (a a' : @DC.Rarg DCMA' DCMlt' DCMiota_A DCMmid DCMiota_lt DCMmid_l DCMmid_r DC.SI.A')
  (x : A)
  (ar : rel' DC.SI.A' (DC.S'.iota_A x) a)
  (ar' : rel' DC.SI.A' (DC.S'.iota_A x) a') : a = a' :=
begin
  cases ar,
end

end