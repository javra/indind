import ..main

meta def e_C : expr := expr.local_const `C `C bidef `(Type)
meta def e_T : expr := expr.local_const `T `T bidef $
  expr.pi `Γ bidef e_C `(Type)
meta def e_nil : expr := expr.local_const `nil `nil bidef e_C
meta def e_ext : expr := expr.local_const `ext `ext bidef $
  expr.pi `Γ bidef e_C $
  expr.pi `A bidef (expr.app e_T (expr.var 0)) e_C
meta def e_unit : expr := expr.local_const `unit `unit bidef $
  expr.pi `Γ bidef e_C (expr.app e_T (expr.var 0))
meta def e_pi : expr := expr.local_const `pi `pi bidef $
  expr.pi `Γ bidef e_C $
  expr.pi `A bidef (expr.app e_T (expr.var 0)) $
  expr.pi `B bidef (expr.app e_T (expr.app (expr.app e_ext (expr.var 1)) (expr.var 0))) $
  expr.app e_T (expr.var 2)
meta def e_npi : expr := expr.local_const `npi `npi bidef $
  expr.pi `Γ bidef e_C $
  expr.pi `f bidef (expr.pi `f bidef (expr.const `nat []) (expr.app e_T (expr.var 1))) $
  expr.app e_T (expr.var 1)

meta def sorts : list expr := [e_C, e_T]
meta def constrs : list expr := [e_nil, e_ext, e_unit, e_pi, e_npi]

def bar : Type :=
begin
  indind.add_ind_helpers `nCT sorts constrs level.zero,
  exact unit,
end


meta instance: has_repr expr := ⟨expr.to_string⟩

#print nCT.Prealg.mk
#print nCT.Alg
#print nCT.SI
#print nCT.S'
#print nCT.Sw_argtype
#print nCT.Sw
