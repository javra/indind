import ..main

meta def e_C : expr := expr.local_const `C `C bidef `(Type)
meta def e_T : expr := expr.local_const `T `T bidef $
  expr.pi `Γ bidef e_C `(Type)
meta def e_nil : expr := expr.local_const `C.nil `nil bidef e_C
meta def e_ext : expr := expr.local_const `C.ext `ext bidef $
  expr.pi `Γ bidef e_C $
  expr.pi `A bidef (expr.app e_T (expr.var 0)) e_C
meta def e_unit : expr := expr.local_const `T.unit `unit bidef $
  expr.pi `Γ bidef e_C (expr.app e_T (expr.var 0))
meta def e_pi : expr := expr.local_const `T.pi `pi bidef $
  expr.pi `Γ bidef e_C $
  expr.pi `A bidef (expr.app e_T (expr.var 0)) $
  expr.pi `B bidef (expr.app e_T (expr.app (expr.app e_ext (expr.var 1)) (expr.var 0)))
    (expr.app e_T (expr.var 2))

meta def sorts : list expr := [e_C, e_T]
meta def constrs : list expr := [e_nil, e_ext, e_unit, e_pi]

set_option trace.app_builder true
set_option pp.proofs true
def bar : Type :=
begin
  indind.add_ind_helpers `CT sorts constrs level.zero,
  exact unit,
end

meta instance: has_repr expr := ⟨expr.to_string⟩
meta instance: has_repr name := ⟨name.to_string⟩

#print CT.Prealg.mk
#print CT.Alg
#print CT.SI
#print CT.S'
#print CT.Sarg
#print CT.Sw
#print C
#print T
#print C.nil
#print C.ext
#print T.unit
#print T.pi