
universes u v w
def cast_heq_cast {α β γ δ : Sort u} {h₁ : α = β} {h₂ : γ = δ} (a : α) (g : γ)
  (p : a == g) : (cast h₁ a == cast h₂ g) :=
begin
  induction h₁, induction h₂, exact p
end

def heq_iff_cast {α β : Sort u} (a : α) (b : β) :
  (a == b) ↔ ∃ (q : α = β), cast q a = b :=
begin
  constructor,
  { intro p, induction p, exact ⟨rfl, rfl⟩ },
  { intro e, cases e with q r, induction r,
    induction q, apply heq.symm, apply cast_heq }
end

def heq_iff_cast_right {α β : Sort u} (a : α) (b : β) :
  (a == b) ↔ ∃ (q : β = α), a = cast q b :=
begin
  constructor,
  { intro p, induction p, exact ⟨rfl, rfl⟩ },
  { intro e, cases e with q r,
    induction q, subst r, apply cast_heq }
end

def hfunext_dom {A A' B : Type} (p : A = A') {f : A → B} {g : A' → B}
  (q : Π x, f x = g (cast p x)) : f == g :=
begin
  cases p, fapply heq_of_eq, fapply funext, intro x,
  exact q x
end

def hfunext_cod {A : Type} {B B' : A → Type}
  (p : B = B') {f : Π x, B x} {g : Π x, B' x}
  (q : Π x, f x == g x) : f == g :=
begin
  cases p,
  fapply heq_of_eq, apply funext, intro x,
  exact eq_of_heq (q x)
end

def hfunext_both {A A' : Sort u} {B : A → Sort v} {B' : A' → Sort v}
  (p : A = A') (q : Π x, B x == B' (cast p x))
  {f : Π x, B x} {g : Π x, B' x} (r : Π x, f x == g (cast p x)) : f == g :=
begin
  induction p, dsimp[cast] at *,
  have q' : B = B', fapply funext, intro x, apply eq_of_heq, exact q x,
  induction q',
  fapply heq_of_eq, fapply funext, intro x, fapply eq_of_heq, exact r x
end

def eq_mpr_eq_cast (α β : Sort u) (a : α) (q : β = α) :
  eq.mpr q a = cast (eq.symm q) a :=
by induction q; refl

def cast_congr_arg {A : Sort u} {B : A → Sort v} {C : A → Sort w} {a a' : A} (p : a = a')
  (f : Π {a}, B a → C a) {b : B a} :
  cast (congr_arg C p) (f b) = f (cast (congr_arg B p) b) :=
by { induction p, refl }

def cast_heq_self {A : Sort u} {B : A → Sort v}  {a a' : A}
  (p : a = a') {b : B a} :
  cast (congr_arg B p) b == b :=
by { induction p, refl }

namespace expr

meta def is_local_const_of : expr → name → bool
| (local_const m n' bi t) n := n = n'
| e                       n := ff

meta def is_lapp_of (e : expr) (n : name) : bool :=
is_local_const_of (get_app_fn e) n

meta def is_app_of_local_const : expr → bool
| (app f x)              := is_app_of_local_const f
| (local_const m n bi t) := tt
| _                      := ff

meta def collect_headers : expr → list expr
| (pi n bi h b)          := (expr.local_const n n bi h) :: (collect_headers b)
| (local_const m n bi t) := collect_headers t
| e                      := []

meta def apps : expr → list expr → expr
| e []      := e
| e (x::xs) := apps (expr.app e x) xs

meta def collect_local_names : expr → list name
| (pi n bi h b)          := collect_local_names h ++ collect_local_names b
| (app f x)              := collect_local_names f ++ collect_local_names x
| (local_const m n bi t) := [m]
| (var n)                := []
| e                      := []

meta def locals_to_consts : expr → expr :=
λ f, f.replace $ λ e d, match e with
                        | (local_const m n bi t) := some $ const m []
                        | _ := none
                        end

meta def make_local_implicit : expr → expr
| (local_const m n bi t) := expr.local_const m n binder_info.implicit t
| e                      := e

end expr