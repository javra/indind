import .util

universe u
open punit tactic

section
parameters (A : Type u) (lt : A → A → Type u)

structure Alg :=
  (A' : Type u)
  (lt' : Π (x y : A'), Type u)
  (iota_A : Π (a : A), A')
  (mid : Π x y (p : lt' x y), A')
  (iota_lt : Π a b, lt a b → lt' (iota_A a) (iota_A b))
  (mid_l : Π x y p, lt' x (mid x y p))
  (mid_r : Π x y p, lt' (mid x y p) y)

structure Alg' :=
  (A' : Type u)
  (lt' : Type u)
  (iota_A : Π (a : A), A')
  (mid : Π (x y : A') (p : lt'), A')
  (iota_lt : Π (a b : A) (p : lt a b), lt')
  (mid_l : Π (x y : A') (p : lt'), lt')
  (mid_r : Π (x y : A') (p : lt'), lt')

inductive SI : Type u
| A' : SI
| lt' : SI

inductive S'0 : SI → Type u
| iota_A : A → S'0 SI.A'
| mid : Π (x y : S'0 SI.A') (p : S'0 SI.lt'), S'0 SI.A'
| iota_lt : Π (a b : A) (p : lt a b), S'0 SI.lt'
| mid_l : Π (x y : S'0 SI.A') (p : S'0 SI.lt'), S'0 SI.lt'
| mid_r : Π (x y : S'0 SI.A') (p : S'0 SI.lt'), S'0 SI.lt'

def S' : Alg' :=
{ A' := S'0 SI.A',
  lt' := S'0 SI.lt',
  iota_A := S'0.iota_A,
  mid := S'0.mid,
  iota_lt := S'0.iota_lt,
  mid_l := S'0.mid_l,
  mid_r := S'0.mid_r }

inductive Sarg : SI → Type u
| A' : Sarg SI.A'
| lt' : S'.A' →  S'.A' → Sarg SI.lt'

inductive Sw : Π b, S'0 b → Sarg b → Prop
| iota_A : Π x, Sw SI.A' (S'0.iota_A x) Sarg.A'
| mid : Π x y p, Sw SI.A' x Sarg.A' → Sw SI.A' y Sarg.A' → Sw SI.lt' p (Sarg.lt' x y) → Sw SI.A' (S'0.mid x y p) Sarg.A'
| iota_lt : Π a b p, Sw SI.lt' (S'0.iota_lt a b p) (Sarg.lt' (S'0.iota_A a) (S'0.iota_A b))
| mid_l : Π x y p, Sw SI.A' x Sarg.A' → Sw SI.A' y Sarg.A' → Sw SI.lt' p (Sarg.lt' x y)
    → Sw SI.lt' (S'0.mid_l x y p) (Sarg.lt' x (S'0.mid x y p))
| mid_r : Π x y p, Sw SI.A' x Sarg.A' → Sw SI.A' y Sarg.A' → Sw SI.lt' p (Sarg.lt' x y)
    → Sw SI.lt' (S'0.mid_r x y p) (Sarg.lt' (S'0.mid x y p) y)

def S : Alg :=
{ A' := Σ' x, Sw SI.A' x Sarg.A',
  lt' := λ x y, Σ' p, Sw SI.lt' p (Sarg.lt' x.1 y.1),
  iota_A := λ x, ⟨S'0.iota_A x, Sw.iota_A x⟩,
  mid := λ a b p, ⟨S'0.mid a.1 b.1 p.1, Sw.mid _ _ _ a.2 b.2 p.2⟩,
  iota_lt := λ x y p, ⟨S'0.iota_lt x y p, Sw.iota_lt x y p⟩,
  mid_l := λ x y p, ⟨S'0.mid_l x.1 y.1 p.1, Sw.mid_l _ _ _ x.2 y.2 p.2⟩,
  mid_r := λ x y p, ⟨S'0.mid_r x.1 y.1 p.1, Sw.mid_r _ _ _ x.2 y.2 p.2⟩ }

-- That relation
section relation
parameters (M : Alg)
include M

inductive rel_arg : SI → Type u
| A' : M.A' → rel_arg SI.A'
| lt' : Π α β, M.lt' α β → rel_arg SI.lt'

inductive rel : Π b, S'0 b → rel_arg b → Prop
| iota_A : Π x, rel SI.A' (S'0.iota_A x) (rel_arg.A' (M.iota_A x))
| mid : Π x y p α β pp 
    (xr : rel SI.A' x (rel_arg.A' α))
    (yr : rel SI.A' y (rel_arg.A' β))
    (pr : rel SI.lt' p (rel_arg.lt' α β pp)),
    rel SI.A' (S'0.mid x y p) (rel_arg.A' (M.mid α β pp))
| iota_lt : Π a b p,
    rel SI.lt' (S'0.iota_lt a b p) (rel_arg.lt' _ _ (M.iota_lt a b p))
| mid_l : Π x y p α β pp
    (xr : rel SI.A' x (rel_arg.A' α))
    (yr : rel SI.A' y (rel_arg.A' β))
    (pr : rel SI.lt' p (rel_arg.lt' α β pp)),
    rel SI.lt' (S'0.mid_l x y p) (rel_arg.lt' _ _ (M.mid_l α β pp))
| mid_r : Π x y p α β pp
    (xr : rel SI.A' x (rel_arg.A' α))
    (yr : rel SI.A' y (rel_arg.A' β))
    (pr : rel SI.lt' p (rel_arg.lt' α β pp)),
    rel SI.lt' (S'0.mid_r x y p) (rel_arg.lt' _ _ (M.mid_r α β pp))

-- The relation is right-unique
theorem right_unique (b : SI) (x : S'0 b) (a a' : rel_arg b) :
  rel b x a → rel b x a' → a = a' :=
begin
  intros ar ar', induction x with x x y p xh yh ph x y p x y p xh yh ph x y p xh yh ph,
  { cases ar, cases ar', refl },
  { cases ar, cases ar',
    cases xh (rel_arg.A' ar_α) (rel_arg.A' ar'_α) ar_xr ar'_xr,
    cases yh (rel_arg.A' ar_β) (rel_arg.A' ar'_β) ar_yr ar'_yr,
    cases ph (rel_arg.lt' ar_α ar_β ar_pp) (rel_arg.lt' ar_α ar_β ar'_pp) ar_pr ar'_pr,
    refl },
  { cases ar, cases ar', refl },
  { cases ar, cases ar',
    cases xh (rel_arg.A' ar_α) (rel_arg.A' ar'_α) ar_xr ar'_xr,
    cases yh (rel_arg.A' ar_β) (rel_arg.A' ar'_β) ar_yr ar'_yr,
    cases ph (rel_arg.lt' ar_α ar_β ar_pp) (rel_arg.lt' ar_α ar_β ar'_pp) ar_pr ar'_pr,
    refl },
  { cases ar, cases ar',
    cases xh (rel_arg.A' ar_α) (rel_arg.A' ar'_α) ar_xr ar'_xr,
    cases yh (rel_arg.A' ar_β) (rel_arg.A' ar'_β) ar_yr ar'_yr,
    cases ph (rel_arg.lt' ar_α ar_β ar_pp) (rel_arg.lt' ar_α ar_β ar'_pp) ar_pr ar'_pr,
    refl }
end

-- Longer, inductive alternative
/-
inductive rel_existence_motive : Π (b : SI), S'0 b → Type u
| A' : Π (x : S'0 SI.A') 
    (hα :  Π (gx : Sw SI.A' x Sarg.A'), M.A')
    (hrα : Π (gx : Sw SI.A' x Sarg.A'), rel SI.A' x (rel_arg.A' $ hα gx)),
  rel_existence_motive SI.A' x
| lt' : Π (p : S'0 SI.lt') 
    (hα : Π (x y : S'0 SI.A') (gx : Sw SI.A' x Sarg.A') (gy : Sw SI.A' y Sarg.A') (gp : Sw SI.lt' p (Sarg.lt' x y)), M.A')
    (hβ : Π (x y : S'0 SI.A') (gx : Sw SI.A' x Sarg.A') (gy : Sw SI.A' y Sarg.A') (gp : Sw SI.lt' p (Sarg.lt' x y)), M.A')
    (hπ : Π (x y : S'0 SI.A') (gx : Sw SI.A' x Sarg.A') (gy : Sw SI.A' y Sarg.A') (gp : Sw SI.lt' p (Sarg.lt' x y)),
      M.lt' (hα x y gx gy gp) (hβ x y gx gy gp))
    (hrα : Π (x y : S'0 SI.A') (gx : Sw SI.A' x Sarg.A') (gy : Sw SI.A' y Sarg.A') (gp : Sw SI.lt' p (Sarg.lt' x y)),
      rel SI.A' x (rel_arg.A' (hα x y gx gy gp)))
    (hrβ : Π (x y : S'0 SI.A') (gx : Sw SI.A' x Sarg.A') (gy : Sw SI.A' y Sarg.A') (gp : Sw SI.lt' p (Sarg.lt' x y)),
      rel SI.A' y (rel_arg.A' (hβ x y gx gy gp)))
    (hrπ : Π (x y : S'0 SI.A') (gx : Sw SI.A' x Sarg.A') (gy : Sw SI.A' y Sarg.A') (gp : Sw SI.lt' p (Sarg.lt' x y)),
      rel SI.lt' p (rel_arg.lt' _ _(hπ x y gx gy gp))),
  rel_existence_motive SI.lt' p
-/

-- Shorter, recursive alternative
inductive rel_existence_motive_target_A'
  (x : S'0 SI.A') (s : Sarg SI.A') : Type u
| mk : Π (α : M.A') 
         (rα : rel SI.A' x (rel_arg.A' α)),
       rel_existence_motive_target_A'

inductive rel_existence_motive_target_lt' 
  (p : S'0 SI.lt') (x y : S'0 SI.A') : Type u
| mk : Π  (α : M.A') (β : M.A') (π : M.lt' α β)
          (rα : rel SI.A' x (rel_arg.A' α))
          (rβ : rel SI.A' y (rel_arg.A' β))
          (rπ : rel SI.lt' p (rel_arg.lt' α β π)),
        rel_existence_motive_target_lt'

def rel_existence_motive (b : SI) : S'0 b → Type u :=
begin
  cases b,
  exact λ x, Sw SI.A' x Sarg.A' → rel_existence_motive_target_A' x Sarg.A',
  exact λ p, Π x y, Sw SI.A' x Sarg.A' → Sw SI.A' y Sarg.A'
    → Sw SI.lt' p (Sarg.lt' x y) →
    rel_existence_motive_target_lt' p x y,
end

set_option pp.proofs false
def rel_existence (b : SI) (x : S'0 b) : rel_existence_motive b x :=
begin
  induction x with a x y p xh yh ph a bb p x y p xh yh ph x y p xh yh ph;
    dsimp[rel_existence_motive] at *,
  { intro w, fconstructor, exact M.iota_A a, constructor },
  { intro w,
    have xw : Sw SI.A' x Sarg.A', cases w, assumption,
    have yw : Sw SI.A' y Sarg.A', cases w, assumption,
    have pw : Sw SI.lt' p (Sarg.lt' x y), cases w, assumption,
    cases xh xw with α rα,
    cases yh yw with β rβ,
    cases ph x y xw yw pw,
    cases right_unique SI.A' x (rel_arg.A' α) (rel_arg.A' α_1) rα rα_1,
    cases right_unique SI.A' y (rel_arg.A' β) (rel_arg.A' β_1) rβ rβ_1,
    fconstructor,
    exact M.mid α β π,
    constructor, assumption, assumption, assumption },
  { intros x y xw yw w,
    have : x = S'0.iota_A a, cases w, refl, cases this,
    have : y = S'0.iota_A bb, cases w, refl, cases this,
    constructor, constructor, constructor, constructor },
  { intros x' y' x'w y'w w,
    have : x = x', cases w, refl, cases this,
    have : y' = S'0.mid x y p, cases w, refl, cases this,
    have xw : Sw SI.A' x Sarg.A', cases w, assumption,
    have yw : Sw SI.A' y Sarg.A', cases w, assumption,
    have pw : Sw SI.lt' p (Sarg.lt' x y), cases w, assumption,
    cases xh xw with α rα,
    cases yh yw with β rβ, 
    cases ph x y xw yw pw,
    cases right_unique SI.A' x (rel_arg.A' α) (rel_arg.A' α_1) rα rα_1,
    cases right_unique SI.A' y (rel_arg.A' β) (rel_arg.A' β_1) rβ rβ_1,
    fconstructor, exact α, exact M.mid α β π, exact M.mid_l _ _ _,
    assumption, constructor, assumption, assumption, assumption, 
    constructor, assumption, assumption, assumption },
  { intros x' y' x'w y'w w,
    have : x' = S'0.mid x y p, cases w, refl, cases this,
    have : y' = y, cases w, refl, cases this,
    have xw : Sw SI.A' x Sarg.A', cases w, assumption,
    have yw : Sw SI.A' y Sarg.A', cases w, assumption,
    have pw : Sw SI.lt' p (Sarg.lt' x y), cases w, assumption,
    cases xh xw with α rα,
    cases yh yw with β rβ, 
    cases ph x y xw yw pw,
    cases right_unique SI.A' x (rel_arg.A' α) (rel_arg.A' α_1) rα rα_1,
    cases right_unique SI.A' y (rel_arg.A' β) (rel_arg.A' β_1) rβ rβ_1,
    fconstructor, exact M.mid α β π, exact β, exact M.mid_r _ _ _,
    constructor, assumption, assumption, assumption, assumption,
    constructor, assumption, assumption, assumption }
end

end relation

section recursor
  parameter (M : Alg)
  include M

set_option pp.proofs true
def A'.elim (x : S'0 SI.A') (xw : Sw SI.A' x Sarg.A') : M.A' :=
begin
  cases rel_existence M SI.A' x xw, exact α,
end

def lt'.elim (x y : S'0 SI.A') (p : S'0 SI.lt')
  (xw : Sw SI.A' x Sarg.A') (yw : Sw SI.A' y Sarg.A')
  (pw : Sw SI.lt' p (Sarg.lt' x y)) :
  M.lt' (A'.elim x xw) (A'.elim y yw) :=
begin
  dsimp[A'.elim] at *,
  cases rel_existence M SI.A' x xw with α αr,
  cases rel_existence M SI.A' y yw with β βr,
  cases rel_existence M SI.lt' p x y xw yw pw with αp βp π αpr βpr pp,
  cases right_unique M SI.A' x (rel_arg.A' α) (rel_arg.A' αp) αr αpr,
  cases right_unique M SI.A' y (rel_arg.A' β) (rel_arg.A' βp) βr βpr,
  assumption
end

theorem A'.iota_A.elim (a : A) :
  A'.elim (S'0.iota_A a) (Sw.iota_A a) = M.iota_A a := rfl

set_option pp.proofs true
theorem A'.mid.elim (x y : S'0 SI.A' ) (p : S'0 SI.lt')
  (xw : Sw SI.A' x Sarg.A') (yw : Sw SI.A' y Sarg.A')
  (pw : Sw SI.lt' p (Sarg.lt' x y)):
  A'.elim (S'0.mid x y p) (Sw.mid x y p xw yw pw)
  = M.mid (A'.elim x xw) (A'.elim y yw) (lt'.elim x y p xw yw pw) :=
begin
  dsimp[A'.elim, lt'.elim],
  cases rel_existence M SI.A' x xw with α αr,
  cases rel_existence M SI.A' y yw with β βr,
  cases rel_existence M SI.lt' p x y xw yw pw with αp pr,
  cases right_unique M SI.A' x _ _ αr rα,
  cases right_unique M SI.A' y _ _ βr rβ,
  cases rel_existence M SI.A' (S'0.mid x y p) (Sw.mid _ _ _ xw yw pw) with γ γr,
  change γ = M.mid _ _ π,
  have := right_unique M SI.A' (S'0.mid x y p) (rel_arg.A' γ) _ γr (rel.mid _ _ _ _ _ _ αr βr rπ),
  injection this,
end

theorem lt'.iota_lt.elim (a b : A) (p : lt a b) :
  lt'.elim _ _ (S'0.iota_lt a b p) (Sw.iota_A a) (Sw.iota_A b) (Sw.iota_lt a b p) 
  = M.iota_lt _ _ p :=
begin
  refl
end

theorem lt'.mid_l.elim (x y : S'0 SI.A') (p : S'0 SI.lt')
  (xw : Sw SI.A' x Sarg.A') (yw : Sw SI.A' y Sarg.A') (pw : Sw SI.lt' p (Sarg.lt' x y)) :
  lt'.elim _ _ (S'0.mid_l x y p) xw (Sw.mid x y p xw yw pw) (Sw.mid_l x y p xw yw pw)
  == M.mid_l (A'.elim x xw) (A'.elim y yw) (lt'.elim x y p xw yw pw) :=
begin
  dsimp[A'.elim, lt'.elim],
  cases rel_existence M SI.A' x xw with α αr,
  cases rel_existence M SI.A' y yw with β βr,
  cases rel_existence M SI.lt' p x y xw yw pw with αp pr,
  cases right_unique M SI.A' x _ _ αr rα,
  cases right_unique M SI.A' y _ _ βr rβ,
  dsimp[A'.elim, S] at *,
  cases rel_existence M SI.A' (S'0.mid x y p) (Sw.mid _ _ _ xw yw pw) with γ γr,
  cases right_unique M SI.A' (S'0.mid x y p) _ _ γr (rel.mid _ _ _ _ _ _ αr βr rπ),
  cases rel_existence M SI.lt' (S'0.mid_l x y p) x (S'0.mid x y p) xw 
          (Sw.mid x y p xw yw pw)
          (Sw.mid_l x y p xw yw pw),
  have := right_unique M SI.lt' _ _ _ rπ_1 (rel.mid_l x y p _ _ _ αr βr rπ),
  injection this,
  cases h_2, cases h_1, cases h_3, dsimp,
  change M.mid_l _ _ _ == M.mid_l _ _ _,
  congr,
  { cases rel_existence M SI.A' x xw,
    cases right_unique M SI.A' _ _ _ αr rα_2, refl },
  { cases rel_existence M SI.A' x xw,
    cases right_unique M SI.A' _ _ _ αr rα_2, refl } 
end

theorem lt'.mid_r.elim (x y : S'0 SI.A') (p : S'0 SI.lt')
  (xw : Sw SI.A' x Sarg.A') (yw : Sw SI.A' y Sarg.A') (pw : Sw SI.lt' p (Sarg.lt' x y)) :
  lt'.elim _ _ (S'0.mid_r x y p) (Sw.mid x y p xw yw pw) yw (Sw.mid_r x y p xw yw pw)
  == M.mid_r (A'.elim x xw) (A'.elim y yw) (lt'.elim x y p xw yw pw) :=
begin
  dsimp[A'.elim, lt'.elim],
  cases rel_existence M SI.A' x xw with α αr,
  cases rel_existence M SI.A' y yw with β βr,
  cases rel_existence M SI.lt' p x y xw yw pw with αp pr,
  cases right_unique M SI.A' x _ _ αr rα,
  cases right_unique M SI.A' y _ _ βr rβ,
  dsimp[A'.elim, S] at *,
  cases rel_existence M SI.A' (S'0.mid x y p) (Sw.mid _ _ _ xw yw pw) with γ γr,
  cases right_unique M SI.A' (S'0.mid x y p) _ _ γr (rel.mid _ _ _ _ _ _ αr βr rπ),
  cases rel_existence M SI.lt' (S'0.mid_r x y p) (S'0.mid x y p) y 
          (Sw.mid x y p xw yw pw) yw
          (Sw.mid_r x y p xw yw pw),
  have := right_unique M SI.lt' _ _ _ rπ_1 (rel.mid_r x y p _ _ _ αr βr rπ),
  injection this,
  cases h_2, cases h_1, cases h_3, dsimp,
  change M.mid_r _ _ _ == M.mid_r _ _ _,
  congr,
  { cases rel_existence M SI.A' x xw,
    cases right_unique M SI.A' _ _ _ αr rα_2, refl },
  { cases rel_existence M SI.A' x xw,
    cases right_unique M SI.A' _ _ _ αr rα_2, refl } 
end


end recursor

end