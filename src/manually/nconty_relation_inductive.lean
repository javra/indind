import ..util

universe u
open punit

structure CT :=
  (C : Type u)
  (T : C → Type u)
  (nil : C)
  (ext : Π (Γ : C), T Γ → C)
  (unit : Π (Γ : C), T Γ)
  (pi : Π (Γ : C) (A : T Γ) (B : T (ext Γ A)), T Γ)
  (npi : Π (Γ : C) (As : ℕ → T Γ), T Γ)

structure CTm (M N : CT) :=
  (C : M.C → N.C)
  (T : Π Γ, M.T Γ → N.T (C Γ))
  (nil : C M.nil = N.nil)
  (ext : Π Γ (A : M.T Γ), C (M.ext Γ A) = N.ext (C Γ) (T Γ A))
  (unit : Π Γ, T _ (M.unit Γ) = N.unit (C Γ))
  (pi : Π Γ A B,
    T _ (M.pi Γ A B) = N.pi _ (T _ A) (cast (congr_arg _ (ext _ _)) (T _ B))) --replace this by ==?
  (npi : Π Γ As, T _ (M.npi Γ As) = N.npi _ (λ n, T _ (As n)))

structure CT' :=
  (C : Type u)
  (T : Type u)
  (nil : C)
  (ext : C → T → C)
  (unit : C → T)
  (pi : C → T → T → T)
  (npi : C → (ℕ → T) → T)

inductive S'0 : bool → Type u
| nil : S'0 ff
| ext : S'0 ff → S'0 tt → S'0 ff
| unit : S'0 ff → S'0 tt
| pi : S'0 ff → S'0 tt → S'0 tt → S'0 tt
| npi : S'0 ff → (ℕ → S'0 tt) → S'0 tt

def S' : CT' :=
{ C := S'0 ff,
  T := S'0 tt,
  nil := S'0.nil,
  ext := S'0.ext,
  unit := S'0.unit,
  pi := S'0.pi,
  npi := S'0.npi }

def Sw0_argtype : bool → Type
| ff := unit
| tt := S'.C

inductive Sw : Π b, S'0 b → Sw0_argtype b → Prop
| nil : Sw ff S'0.nil ()
| ext : Π Γ A xΓ, Sw ff Γ xΓ → Sw tt A Γ → Sw ff (S'.ext Γ A) ()
| unit : Π Γ xΓ, Sw ff Γ xΓ → Sw tt (S'.unit Γ) Γ
| pi : Π Γ A B xΓ, Sw ff Γ xΓ → Sw tt A Γ → Sw tt B (S'.ext Γ A) → 
    Sw tt (S'.pi Γ A B) Γ
| npi : Π Γ (As : ℕ → S'0 tt) xΓ,
    Sw ff Γ xΓ → (Π n, Sw tt (As n) Γ) → Sw tt (S'.npi Γ As) Γ

def S : CT :=
{ C := Σ' (Γ : S'.C), Sw ff Γ (),
  T := λ Γ, Σ' (A : S'.T), Sw tt A Γ.1,
  nil := ⟨S'.nil, Sw.nil⟩,
  ext := λ Γ A, ⟨S'.ext Γ.1 A.1, Sw.ext _ _ _ Γ.2 A.2⟩,
  unit := λ Γ, ⟨S'.unit Γ.1, Sw.unit _ _ Γ.2⟩,
  pi := λ Γ A B, ⟨S'.pi Γ.1 A.1 B.1, Sw.pi _ _ _ _ Γ.2 A.2 B.2⟩,
  npi := λ Γ As, ⟨S'.npi Γ.1 (λ n, (As n).1), Sw.npi _ _ _ Γ.2 (λ n, (As n).2)⟩ }

-- That relation
section relation
parameters (M : CT.{u})
include M

def rel_arg : bool → Type u
| ff := M.C
| tt := Σ γ, M.T γ

inductive rel : Π b, S'0 b → rel_arg b → Prop
| nil : rel ff S'0.nil M.nil
| ext : Π Γ A γ a, rel ff Γ γ → rel tt A ⟨γ, a⟩ → rel ff (S'0.ext Γ A) (M.ext γ a)
| unit : Π Γ γ, rel ff Γ γ → rel tt (S'0.unit Γ) ⟨γ, M.unit γ⟩
| pi : Π Γ A B γ a b, rel ff Γ γ → rel tt A ⟨γ, a⟩ → 
    rel tt B ⟨M.ext γ a, b⟩ → rel tt (S'0.pi Γ A B) ⟨γ, M.pi γ a b⟩
| npi : Π Γ (As : ℕ → S'0 tt) γ (as : ℕ → M.T γ), rel ff Γ γ →
    (∀ (n : ℕ), rel tt (As n) ⟨γ, as n⟩) →
    rel tt (S'0.npi Γ As) ⟨γ, M.npi γ as⟩

-- The relation is right-unique
theorem rel_unique (b : bool) (x : S'0 b) (γa γa' : rel_arg b) :
  rel b x γa → rel b x γa' → γa = γa' :=
begin
  intros γar γar', induction x with Γ A Γh Ah Γ Γh Γ A B Γh Ah Bh Γ As Γh Ash,
  { cases γar, cases γar', refl },
  { cases γar with _ _ γ a γr ar,
    cases γar' with _ _ γ' a' γr' ar',
    cases Γh γ γ' γr γr',
    cases Ah ⟨γ, a⟩ ⟨γ, a'⟩ ar ar', refl },
  { cases γar, cases γar',
    cases Γh γar_γ γar'_γ γar_a γar'_a, refl },
  { cases γar with _ _ _ _ _ _ _ _ _ _ _ _ γ a b γr ar br,
    cases γar' with _ _ _ _ _ _ _ _ _ _ _ _ γ' a' b' γr' ar' br',
    cases Γh γ γ' γr γr',
    cases Ah ⟨γ, a⟩ ⟨γ, a'⟩ ar ar',
    cases Bh ⟨M.ext γ a, b⟩ ⟨M.ext γ a, b'⟩ br br', refl },
  { cases γar with _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ γ as γr asr,
    cases γar' with _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ γ' as' γr' asr',
    cases Γh γ γ' γr γr',
    have: as = as', apply funext, intro n, dsimp at *,
      let foo := Ash n ⟨γ, as n⟩ ⟨γ, as' n⟩ (asr n) (asr' n),
      injection foo with _ e, apply eq_of_heq, exact e,
    cases this, refl }
end

def rel_existence_motive (b : bool) : S'0 b → Type u :=
begin
  cases b,
  exact λ Γ, Sw ff Γ () → Σ' γ, rel ff Γ γ,
  exact λ A, Π Γ, Sw ff Γ () → Sw tt A Γ → Σ' γ a, rel ff Γ γ ∧ rel tt A ⟨γ, a⟩
end

def rel_existence (b : bool) (x : S'0 b) : rel_existence_motive b x :=
begin
  induction x with Γ A Γh Ah Γ Γh Γ A B Γh Ah Bh Γ As Γh Ash; dsimp[rel_existence_motive] at *,
  { intro w, exact ⟨M.nil, rel.nil⟩ },
  { intro w,
    have Γw : Sw ff Γ (), cases w, cases w_xΓ, exact w_a,
    have Aw : Sw tt A Γ, cases w, exact w_a_1,
    cases Γh Γw with γ γr,
    cases Ah _ Γw Aw with γa γar, cases γar with a γar, cases γar with γar ar,
    cases rel_unique ff Γ γ γa γr γar,
    exact ⟨M.ext γ a, rel.ext _ _ _ _ γr ar⟩ },
  { intros Γ' Γ'w w,
    have: Γ = Γ', cases w, refl, cases this,
    have Γw : Sw ff Γ (), cases w, exact Γ'w,
    cases Γh Γw with γ γr,
    exact ⟨_, M.unit γ, γr, rel.unit _ _ γr⟩ },
  { intros Γ' Γw pw,
    have: Γ = Γ', cases pw, refl, cases this,
    have Aw : Sw tt A Γ, cases pw, assumption,
    have Bw : Sw tt B (S'.ext Γ A), cases pw, assumption,
    cases Γh Γw with γ γr,
    cases Ah _ Γw Aw with γa γar,
    cases γar with a γar, cases γar with γar ar,
    cases rel_unique ff Γ γ γa γr γar,
    cases Bh _ (Sw.ext Γ A () Γw Aw) Bw with γb γbr,
    cases γbr with b γbr, cases γbr with γbr br,
    cases rel_unique ff _ γb (M.ext γ a) γbr (rel.ext _ _ _ _ γr ar),
    exact ⟨_, M.pi γ a b, γr, rel.pi _ _ _ _ _ _ γr ar br⟩ },
  { intros Γ' Γw pw,
    have: Γ = Γ', cases pw, refl, cases this,
    have Aw : Π n, Sw tt (As n) Γ, cases pw, assumption,
    cases Γh Γw with γ γr,
    have as : Π n, Σ' a : M.T γ, rel tt (As n) ⟨_, a⟩, intro n,
      cases Ash n _ Γw (Aw n) with γa γar,
      cases γar with a γar, cases γar with γar ar,
      cases rel_unique ff Γ γ γa γr γar, exact ⟨a, ar⟩,
    exact ⟨_, M.npi γ (λ n, (as n).1), γr, rel.npi _ _ _ _ γr (λ n, (as n).2)⟩ }
end

end relation

def Con := S.C
def Ty := S.T
def Con.nil := S.nil
def Con.ext := S.ext
def Ty.unit := S.unit
def Ty.pi := S.pi
def Ty.npi := S.npi

section recursor
  parameter (M : CT)
  include M

def Con.elim : Con → M.C
| ⟨Γ, Γw⟩ := (rel_existence M ff Γ Γw).1

def Ty.elim : Π {Γ}, Ty Γ → M.T (Con.elim Γ) :=
begin
  intros Γ A, cases Γ with Γ Γw, cases A with A Aw, dsimp[Con.elim],
  cases rel_existence M ff Γ Γw with γ γr,
  cases rel_existence M tt A Γ Γw Aw with γa ar,
  cases ar with a ar, cases ar with γar ar, dsimp,
  rw rel_unique M ff Γ γ γa γr γar,
  exact a,
end

theorem Con.nil.elim : Con.elim Con.nil = M.nil := rfl

theorem Con.ext.elim (Γ A) :
  Con.elim (Con.ext Γ A) = M.ext (Con.elim Γ) (Ty.elim A) :=
begin
  cases Γ with Γ Γw, cases A with A Aw, dsimp[Con.elim, Ty.elim],
  cases rel_existence M ff Γ Γw with γ γr,
  cases rel_existence M tt A Γ Γw Aw with γa ar,
  cases ar with a ar, cases ar with γar ar,
  cases rel_unique M ff Γ γ γa γr γar,
  dsimp[Con.ext, Con.elim, S] at *,
  cases rel_existence M ff (S'0.ext Γ A) (S._proof_1 ⟨Γ, Γw⟩ ⟨A, Aw⟩) with γ' γ'r,
  apply rel_unique M ff (S'0.ext Γ A), apply γ'r,
  exact rel.ext _ _ _ _ γr ar
end

theorem Ty.unit.elim (Γ : Con) :
  Ty.elim (Ty.unit Γ) = M.unit (Con.elim Γ) :=
begin
  cases Γ with Γ Γw,
  dsimp[Ty.unit, S, Con.elim, Ty.elim],
  cases rel_existence M ff Γ Γw with γ γr,
  cases rel_existence M tt (S'.unit Γ) Γ Γw _ with γa ar,
  cases ar with a ar, cases ar with γar ar,
  cases rel_unique M ff Γ γ γa γr γar,
  injection (rel_unique M tt _ _ _ ar (rel.unit _ _ γr)),
  apply eq_of_heq, assumption,
end

theorem Ty.pi.elim' (Γ : Con) (A : Ty Γ) (B : Ty (Con.ext Γ A)) 
  (B' : M.T (M.ext (Con.elim Γ) (Ty.elim A)))
  (e : B' ==  Ty.elim B) :
  Ty.elim (Ty.pi Γ A B) 
  = M.pi (Con.elim Γ) (Ty.elim A) B' :=
begin
  cases Γ with Γ Γw, cases A with A Aw, cases B with B Bw,
  dsimp[S, Ty.pi, Con.ext, Con.elim, Ty.elim, S, S'] at *,
  cases rel_existence M ff Γ Γw with γ γr,
  cases rel_existence M tt A Γ Γw Aw with γa ar,
  cases ar with a ar, cases ar with γar ar,
  cases rel_unique M ff Γ γ γa γr γar,
  cases rel_existence M ff _ (S._proof_1 ⟨Γ, Γw⟩ ⟨A, Aw⟩) with γe γer,
  cases rel_unique M ff _ (M.ext γ a) γe (rel.ext _ _ _ _ γr ar) γer,
  cases rel_existence M tt B _ (S._proof_1 ⟨Γ, Γw⟩ ⟨A, Aw⟩) Bw  with γb br,
  cases br with b br, cases br with γbr br,
  cases rel_unique M ff _ (M.ext γ a) γb (rel.ext _ _ _ _ γr ar) γbr,
  cases rel_existence M tt (S'0.pi Γ A B) Γ Γw (S._proof_4 ⟨Γ, Γw⟩ ⟨A, Aw⟩ ⟨B, Bw⟩) with γp pr,
  cases pr with p pr, cases pr with γpr pr,
  cases rel_unique M ff Γ γ γp γr γpr,
  injection (rel_unique M tt _ _ _ pr (rel.pi _ _ _ _ _ _ γr ar br)),
  apply eq_of_heq, cases e, assumption
end

theorem Ty.pi.elim (Γ : Con) (A : Ty Γ) (B : Ty (Con.ext Γ A)) :
  Ty.elim (Ty.pi Γ A B) 
  = M.pi (Con.elim Γ) (Ty.elim A) (by {rw ←Con.ext.elim, exact Ty.elim B}) :=
Ty.pi.elim' Γ A B _ 
  (by {symmetry, rw [eq_mpr_eq_cast, heq_iff_cast], constructor, refl })

theorem Ty.npi.elim (Γ : Con) (As : ℕ → Ty Γ) :
  Ty.elim (Ty.npi Γ As) = M.npi _ (λ n, Ty.elim (As n)) :=
begin
  cases Γ with Γ Γw,
  dsimp[Ty.npi, S, Con.elim, Ty.elim],
  cases rel_existence M ff Γ Γw with γ γr,
  cases rel_existence M tt (S'.npi Γ (λ (n : ℕ), (As n).fst)) Γ Γw _ with γp pr,
  cases pr with p pr, cases pr with γpr pr,
  cases rel_unique M ff Γ _ _ γr γpr,
  dsimp at *,
  injection (rel_unique M tt _ _ _ pr (rel.npi _ _ _ _ γr _)),
  { apply eq_of_heq, assumption },
  { intro n, cases pr, dsimp at *, dunfold Ty.elim._proof_1, dsimp,
    cases As n with As' Asw, dsimp,
    cases rel_existence M tt As' Γ Γw Asw with γa ar,
    cases ar with a ar, cases ar with γar ar,
    cases rel_unique M ff Γ γ γa γr γar,
    assumption }
end

def ConTy.elim : CTm S M :=
{ C := Con.elim,
  T := @Ty.elim,
  nil := Con.nil.elim,
  ext := Con.ext.elim,
  unit := Ty.unit.elim,
  pi := Ty.pi.elim,
  npi := Ty.npi.elim }

end recursor

section uniqueness
  parameters (M : CT)
  variables (g : CTm S M)

def motive_unique : Π (b : bool), S'0 b → Prop
| ff Γ := Π (gΓ : (Sw ff Γ ())), rel M ff Γ (g.C ⟨Γ, gΓ⟩)
| tt A := Π (Γ : S.C) (gA : (Sw tt A Γ.1)), rel M tt A ⟨_, g.T _ ⟨A, gA⟩⟩

def elim_unique_aux : Π b x, motive_unique g b x :=
begin
  intros b x, induction x with Γ A Γh Ah Γ Γh Γ A B Γh Ah Bh Γ As Γh Ash,
  { intros gΓ, change rel M ff S'0.nil (g.C S.nil),
    rw g.nil, constructor },
  { intros gΓA, cases gΓA with _ _ x gΓ gA, cases x,
    change rel M ff (S'0.ext _ _) (g.C (S.ext ⟨Γ, gΓ⟩ ⟨A, gA⟩)),
    rw g.ext, constructor, apply Γh, apply Ah },
  { intros Γ gg, cases Γ with Γ Γw, cases gg with _ gΓ,
    change rel M tt (S'0.unit _) ⟨_, g.T _ (S.unit _)⟩,
    rw g.unit, constructor, apply Γh },
  { intros Γ gg, cases Γ with Γ Γw,
    cases gg with _ _ _ _ _ _ _ _ _ _ _ x gΓ gA gB, cases x,
    change rel M tt (S'0.pi _ _ _) ⟨_, g.T _ (S.pi ⟨Γ, gΓ⟩ ⟨A, gA⟩ ⟨B, gB⟩)⟩,
    rw g.pi, constructor, apply Γh, apply Ah, 
    have : rel M tt B ⟨g.C (S.ext ⟨Γ, Γw⟩ ⟨A, gA⟩), g.T _ ⟨B, gB⟩⟩
      = rel M tt B
      ⟨M.ext _ (g.T ⟨Γ, gΓ⟩ ⟨A, gA⟩),
         cast (congr_arg _ (g.ext _ ⟨A, gA⟩)) (g.T (S.ext ⟨Γ, gΓ⟩ ⟨A, gA⟩) ⟨B, gB⟩)⟩,
    { congr, apply g.ext, symmetry, apply cast_heq_self },
    rw ←this, apply Bh },
  { intros Γ gg, cases Γ with Γ Γw,
    cases gg with _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ x gΓ gAs, cases x,
    change rel M tt (S'0.npi _ _) ⟨_, g.T _ (S.npi ⟨Γ, gΓ⟩ (λ n, ⟨As n, gAs n⟩))⟩,
    rw g.npi, constructor, apply Γh, intro n, apply Ash }
end

def rec_unique : g = ConTy.elim M :=
begin
  have eC : g.C = (ConTy.elim M).C,
  { funext Γ, cases Γ with Γ Γw, apply rel_unique M ff Γ,
    apply elim_unique_aux g ff,
    apply elim_unique_aux (ConTy.elim M) ff },
  have eT : g.T == (ConTy.elim M).T,
  { have H : ∀ Γ A, g.T Γ A == (ConTy.elim M).T Γ A,
    { intros Γ A, cases Γ with Γ Γw, cases A with A Aw,
      let foo := rel_unique M ff Γ (g.C ⟨Γ, Γw⟩) ((ConTy.elim M).C ⟨Γ, Γw⟩)
        (elim_unique_aux g ff _ _) (elim_unique_aux (ConTy.elim M) ff _ _),
      let bar := rel_unique M tt A ⟨_, g.T ⟨Γ, Γw⟩ ⟨A, Aw⟩⟩
         ⟨_, ((ConTy.elim M).T ⟨Γ, Γw⟩ ⟨A, Aw⟩)⟩
         (elim_unique_aux g tt _ _ _)
         (elim_unique_aux (ConTy.elim M) tt _ _ _),
      injection bar with _ e },
      cases g, dsimp at *, cases eC,
      apply heq_of_eq, funext Γ A, apply eq_of_heq, apply H },
  cases g, congr,
  apply eC, apply eT,
  { apply cast_heq, apply propext, constructor,
    exact λ _, g_nil, exact λ _, (ConTy.elim M).nil },
  { apply cast_heq, apply propext, constructor,
    exact λ _, g_ext, exact λ _, (ConTy.elim M).ext },
  { apply cast_heq, apply propext, constructor,
    exact λ _, g_unit, exact λ _, (ConTy.elim M).unit },
  { apply cast_heq, apply propext, constructor,
    exact λ _, g_pi, exact λ _, (ConTy.elim M).pi },
  { apply cast_heq, apply propext, constructor,
    exact λ _, g_npi, exact λ _, (ConTy.elim M).npi }
end

end uniqueness